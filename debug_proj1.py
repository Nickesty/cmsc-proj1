import sys
import os
def opt(i,j,a,r,matches):
	#subarray = a[i,j]
	if r[i][j] is not -1:
		return r[i][j]
	if i >= j - 4:
	#	r[i][j] = 0
	#	os.system('clear')
	#       printr(r)
        	#raw_input()
		return 0
		
	#opt(i,j-1,a,r)
	
	maximum = 0
	match = -1
	for t in range(i,j-4):
		value = opt(i,t-1,a,r,matches)+opt(t+1,j-1,a,r,matches)
		if isMatching(a[t],a[j]):
			value += 1
		if value > maximum:
			maximum = value
			if isMatching(a[t],a[j]):
				match = t
	if maximum > opt(i,j-1,a,r,matches):
		r[i][j] = maximum
		matches[i][j] = match
	else:
		r[i][j] = opt(i,j-1,a,r,matches)
		matches[i][j] = matches[i][j-1]
	
	#os.system('clear')
	#printr(r)
	#raw_input()
	return r[i][j]

def matches(i,j,a):
	for t in range(i,j-4):
		if match(a[t],a[j]):
			return True
	return False

def isMatching(x,y):
	if x == "H" and y == "G":
		return True
	if x == "G" and y == "H":
		return True
	if x == "W" and y == "T":
		return True
	if x == "T" and y == "W":
		return True
	return False

def printr(r):
	for row in r:
                for num in row:
                        print("%2d"%( num )),
                print
def extractMatches(matches,r):
	i = 0
	s = []
	for j in range(len(r)-1,-1,-1): #Recurse backwards through r
		if r[i][j] == 0 or r[i][j] == -1:
			return s
		if r[i][j] == r[i][j-1]:
                        continue
		s.append((matches[i][j]+1,j+1))
		i = matches[i][j] + 1
	return s

def main():
	f = open(sys.argv[1],"r")
	filestring = f.read().upper().replace(",","").replace(" ","").strip()
	a = list(filestring)
	r = []
	matches = []
	for i in range(len(a)):
		r.append([-1]*len(a))	
		matches.append([-1]*len(a))
	print opt(0,len(a)-1,a,r,matches)
	print extractMatches(matches,r)
	for fan in a:
		print("%2c"%(fan)),
	print
	printr(r)
	print 
	printr(matches)
	#print r[11][16]
	#print r[10][17]
	#print r[7][12]
main()

import sys

def opt(i,j,a,r,matches):

	if r[i][j] is not -1: 	#If r[i][j] has already been computed
		return r[i][j] 	#Return the computed value

	if i >= j - 4:		#If i is greater than or equal to j-4 (to comply with the requirments that bends must be smaller than 4)
		return 0	#Return 0, since it can not be matched
		
	maximum = 0
	match = -1

	for t in range(i,j-4):
		value = opt(i,t-1,a,r,matches) + opt(t+1,j-1,a,r,matches)
		if isMatching(a[t],a[j]):
			value += 1
		if value > maximum:
			maximum = value
			if isMatching(a[t],a[j]):
				match = t

	if maximum > opt(i,j-1,a,r,matches):
		r[i][j] = maximum
		matches[i][j] = match
	else:
		r[i][j] = opt(i,j-1,a,r,matches)
		matches[i][j] = matches[i][j-1]
	
	return r[i][j]

def isMatching(x,y):
	if x == "H" and y == "G":
		return True
	if x == "G" and y == "H":
		return True
	if x == "W" and y == "T":
		return True
	if x == "T" and y == "W":
		return True
	return False

def extractMatches(matches,r):
	i = 0
	s = []

	for j in range(len(r)-1,-1,-1): #Recurse backwards through r
		if r[i][j] == 0 or r[i][j] == -1:
			return s
		if r[i][j] == r[i][j-1]:
                        continue
		s.append((matches[i][j]+1,j+1))
		i = matches[i][j] + 1

	return s

def main():
	f = open(sys.argv[1],"r")
	filestring = f.read().upper().replace(",","").replace(" ","").strip()
	a = list(filestring)
	r = []
	matches = []
	for i in range(len(a)):
		r.append([-1]*len(a))	
		matches.append([-1]*len(a))
	print opt(0,len(a)-1,a,r,matches)
	print extractMatches(matches,r)
main()
